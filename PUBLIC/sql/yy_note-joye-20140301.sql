/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Version : 50611
 Source Host           : localhost
 Source Database       : yyot

 Target Server Version : 50611
 File Encoding         : utf-8

 Date: 03/01/2014 23:03:58 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `yy_note`
-- ----------------------------
DROP TABLE IF EXISTS `yy_note`;
CREATE TABLE `yy_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(500) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `yy_note`
-- ----------------------------
BEGIN;
INSERT INTO `yy_note` VALUES ('16', '能否在公众号上与用户建立关系，与内容是否原创关系不大。原创内容就一定能与用户建立关系吗？摘编的公众号就不能建立关系？错。长篇原创内容让读者欣赏，但看不到他们的人；关系是靠“互动”来完成的，而非内容。', '0000-00-00 00:00:00'), ('17', '能否在公众号上与用户建立关系，与内容是否原创关系不大。原创内容就一定能与用户建立关系吗？摘编的公众号就不能建立关系？错。长篇原创内容让读者欣赏，但看不到他们的人；关系是靠“互动”来完成的，而非内容。', '0000-00-00 00:00:00'), ('21', '如果从发展用户的角度看，内容一定要原创吗？不是。有很多办法可以发展用户，不一定要用原创这种比较笨拙的办法。如果从用户的角度想，内容一定要原创吗？也不是。他们需要对他们有价值的“鸡蛋”，而不太管谁是“鸡', '0000-00-00 00:00:00'), ('22', '能否在公众号上与用户建立关系，与内容是否原创关系不大。原创内容就一定能与用户建立关系吗？摘编的公众号就不能建立关系？错。长篇原创内容让读者欣赏，但看不到他们的人；关系是靠“互动”来完成的，而非内容。\n', '0000-00-00 00:00:00'), ('23', '我之所以还分享文章，是出于两个考虑：一是如果只分享绘本，被分享转发的次数就很少，如果有好的文章想配合，就有很多分享转发，用户发展就比较快；二是出于“经典绘本”目的，致力于家庭关系建造。我希望借着亲子共读绘本培养好的亲子关系，希望借助这些文章有助于建造好的家庭关系。精选文章要眼光和时间。', '0000-00-00 00:00:00');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
