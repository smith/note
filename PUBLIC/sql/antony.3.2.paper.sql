-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 03 月 02 日 08:48
-- 服务器版本: 5.5.35
-- PHP 版本: 5.3.10-1ubuntu3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `note`
--

-- --------------------------------------------------------

--
-- 表的结构 `paper`
--

CREATE TABLE IF NOT EXISTS `paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- 转存表中的数据 `paper`
--

INSERT INTO `paper` (`id`, `content`, `timestamp`) VALUES
(1, 'Hello ThinkPHP', '2014-02-27 00:00:00'),
(2, '第一帖', '2014-02-27 00:00:00'),
(3, 'Very Nice', '2014-02-27 16:58:34'),
(4, 'Very Nice', '2014-02-27 16:58:34'),
(5, 'Very Nice', '2014-02-27 16:58:34'),
(6, 'Very Nice', '2014-02-27 16:58:34'),
(7, 'Very Nice', '2014-02-27 16:58:34'),
(8, 'Very Nice', '2014-02-27 16:58:34'),
(9, 'Very Nice', '2014-02-27 00:00:00'),
(10, 'I''m not sleepy！', '0000-00-00 00:00:00'),
(11, '接着再战！', '0000-00-00 00:00:00'),
(12, '不痛不快！', '0000-00-00 00:00:00'),
(13, '哈哈', '0000-00-00 00:00:00'),
(14, '哈哈', '0000-00-00 00:00:00'),
(15, '终于等到你', '2014-02-27 23:58:00'),
(16, '每天走在疯狂的大街上', '2014-02-28 00:30:00'),
(17, '就当我为遇见你伏笔', '2014-02-28 00:31:00'),
(18, 'Very Nice', '0000-00-00 00:00:00'),
(19, 'Very Nice', '0000-00-00 00:00:00'),
(20, '你是我的信仰', '0000-00-00 00:00:00'),
(21, '开往城市边缘开', '0000-00-00 00:00:00'),
(22, '你把我灌醉', '0000-00-00 00:00:00'),
(23, '门前老树张新芽，院里枯木又开花', '0000-00-00 00:00:00'),
(24, '转眼间只剩下一堆bug啊', '0000-00-00 00:00:00'),
(25, 'bug 啊', '0000-00-00 00:00:00'),
(26, '也许有天会情不自禁', '0000-00-00 00:00:00'),
(27, '你总是轻声的说黑夜有我', '0000-00-00 00:00:00'),
(28, '多少人走着却困在原地', '0000-00-00 00:00:00'),
(29, '我该如何存在', '2014-02-27 17:09:33'),
(30, '多少次幸福却心如刀绞', '2014-02-27 17:10:13'),
(31, '难以忘记初次见你，一双美丽的眼睛', '2014-02-27 17:17:41'),
(32, '难以忘记初次见你，一双美丽的眼睛', '2014-02-27 17:20:58'),
(33, '难以忘记初次见你，一双美丽的眼睛', '2014-02-27 17:24:30'),
(34, '难以忘记初次见你，一双美丽的眼睛', '2014-02-27 17:25:05'),
(35, '偷偷地', '2014-02-28 06:54:48'),
(36, 'aaa', '2014-03-01 15:40:16'),
(37, 'bbb', '2014-03-02 08:32:48');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
