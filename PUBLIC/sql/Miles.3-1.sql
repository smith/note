/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50534
Source Host           : localhost:3306
Source Database       : mysql

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2014-02-27 01:14:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `miles_note`
-- ----------------------------
DROP TABLE IF EXISTS `miles_note`;
CREATE TABLE `miles_note` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of miles_note
-- ----------------------------
INSERT INTO `miles_note` VALUES ('2', '444rrrrr');
INSERT INTO `miles_note` VALUES ('3', '1123');
INSERT INTO `miles_note` VALUES ('5', '3333');
INSERT INTO `miles_note` VALUES ('6', '114');
INSERT INTO `miles_note` VALUES ('7', '333');
INSERT INTO `miles_note` VALUES ('8', '22');
INSERT INTO `miles_note` VALUES ('9', 'tt');
INSERT INTO `miles_note` VALUES ('10', '111r');
INSERT INTO `miles_note` VALUES ('11', '');
INSERT INTO `miles_note` VALUES ('12', 'p');
INSERT INTO `miles_note` VALUES ('13', '');
INSERT INTO `miles_note` VALUES ('14', '');
INSERT INTO `miles_note` VALUES ('15', '3333');
INSERT INTO `miles_note` VALUES ('17', '555555');
