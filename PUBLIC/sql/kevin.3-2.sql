/*
Navicat MySQL Data Transfer

Source Server         : thinkphp
Source Server Version : 50534
Source Host           : localhost:3306
Source Database       : note

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2014-03-04 21:23:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `think_note`
-- ----------------------------
DROP TABLE IF EXISTS `think_note`;
CREATE TABLE `think_note` (
  `noteId` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '笔记编码',
  `noteItem` varchar(300) NOT NULL COMMENT '笔记内容',
  `createDate` datetime NOT NULL COMMENT '创建时间',
  `alterDate` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`noteId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of think_note
-- ----------------------------
INSERT INTO `think_note` VALUES ('0000000001', '#Bread 082th#1.看了下天气，这一周都将在阴雨中度过，明天一定记得带伞，不然又会像今天一样：一会变成了狗一会变成鸡；2.thinkPHP、onethink、ajax、jquery，come on baby @周毅 @王力 @王安 @史西征 ～～', '2014-02-28 00:20:04', '2014-02-28 00:20:04');
INSERT INTO `think_note` VALUES ('0000000002', '#Bread 083th#万事开头难，别发牢骚，不要气急败坏，须知好的开始就是成功的一半！所以该吃吃，该睡睡，明天继续。所有问题都会解决，但在这之前：需要时间，需要耐心，更需要努力工作，cheer up，you can！', '2014-02-28 00:20:37', '2014-02-28 00:20:37');
INSERT INTO `think_note` VALUES ('0000000003', '#Bread 084th#\"I have four children,two boys and two girls\"早上上班等电梯，一位操着纯正美式口音的外籍中年男士对一位天朝女士如是说道。听到这话的中年女士不自觉发出一阵长长的感叹，鄙人也不禁感叹：生这么多孩子，难道真的不会被查水表？还有，这日子怎么过啊？！但是再端详一下这位仪表堂堂、衣着高端的男士，满满的都是自豪，哪里有丝毫为生活发愁的样子……', '2014-02-28 00:22:20', '2014-02-28 00:22:20');
INSERT INTO `think_note` VALUES ('0000000004', '#Bread 085th#一边嚼着口香糖，一边敲代码，根本停不下来，NOTE TO DO LIST，I am about  to  complete the mission. in time? of course, No trouble to baby smith--our dear PM～～', '2014-02-28 00:54:35', '2014-02-28 00:54:35');
INSERT INTO `think_note` VALUES ('0000000013', '人生的路上，我们都在奔跑，我们总是在赶超一些人，也总是在被一些人去超越。人生的秘诀，就是寻找一种最适合自己的速度，莫因疾进而不堪重荷，莫要因迟缓而去空耗生命。', '2014-02-28 16:33:57', '2014-02-28 16:33:57');
INSERT INTO `think_note` VALUES ('0000000014', '毛泽东的群众路线是骗人的口号。有特权的统治者和群众打成一片，以此博得群众的拥护，维护他们的特权。他们手中的特权随时随地可以翻脸不认人，随便地整人。现代社会的组织原则是取消特权，不在乎是不是和群众打成了一片。大家尊重同样的法律，没有例外。统治者犯法也要受法律的制裁，但薄熙来就不同。', '2014-02-28 16:36:04', '2014-02-28 16:36:04');
INSERT INTO `think_note` VALUES ('0000000015', 'TMD的，HTC ONE 手机这回坑苦了我！昨天夜里睡觉的时候手机死机，定的闹钟没有响，起床晚了，错过了一件很重要的事情。', '2014-02-28 16:36:24', '2014-02-28 16:36:24');
INSERT INTO `think_note` VALUES ('0000000016', '突然想起了考研的那段时间，逼自己看书，看到吐，看到神经痛，看到我不得不歇斯底里地发泄才能觉得自己是在活着，那种绝境，我打定注主意，不要再受。', '2014-02-28 16:37:14', '2014-02-28 16:37:14');
