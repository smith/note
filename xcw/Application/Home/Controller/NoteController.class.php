<?php
namespace Home\Controller;
use Think\Controller;
class NoteController extends Controller {
  public function index() {
  	// echo "Note string";
  	$Note = D('Note');
  	$r = $Note->order('createDate desc')->select();
  	// $this->ajaxReturn($r);
  	$this->assign('note',$r);
  	$this->display();
  }

  public function addItem(){
  	$Note = M("Note");
  	if(""==$_POST['noteItem'])
  	{
  		echo "<script>alert('输入为空,请重新输入！'); history.go(-1)</script>";
  		return;
  	}
  	$Note->noteItem = $_POST['noteItem'];
  	$Note->createDate = date('Y-m-d H:i:s');
  	$Note->alterDate = date('Y-m-d H:i:s');
  	$Note->add();
  	$this-> redirect('index');
  }

  public function updateItem(){
    $Note = M("Note");
    $Note->noteItem = $_POST['noteItems'];
    $Note->alterDate = date('Y-m-d H:i:s');
    $Note->where('noteId='.$_POST['noteIds'])->save(); 
    $this-> redirect('index');
  }

  public function deleteItem(){
    $Note = M("Note");
    $Note->where('noteId='.$_GET['noteId'])->delete();
    $this-> redirect('index');
  }
}
