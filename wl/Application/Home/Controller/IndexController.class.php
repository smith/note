<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
	// $this->show('Hello World!');
	// echo "niao";
    	$Note = M("Note"); // 实例化User对象
		$a = $Note->select(); 
		// var_dump($a);


		$this->assign('Note',$a);


		$this->display();

    }

    public function add()
    {
    	$Note = D("Note"); // 实例化User对象
		if (!$Note->create()){ // 创建数据对象
		     // 如果创建失败 表示验证没有通过 输出错误提示信息
		     exit($Note->getError());
		}else{
		     // 验证通过 写入新增数据
		     $Note->add();

		     //设置成功后跳转页面的地址，默认的返回页面是$_SERVER['HTTP_REFERER']
    		// $this->success('新增成功', 'Index/index');
		     //重定向到New模块的Category操作
			$this->redirect('Index/index');
		}

    }

    public function delete()
    {
    	$Note = M("Note"); // 实例化User对象
    	$id = $_POST["ID"];
    	//echo $id;

		if (!$Note->delete($id)){
		     exit($Note->getError());
		}else{

		    $Note->delete();
			$this->redirect('Index/index');

		}
    }

    public function update()
    {
    	$Note = M("Note"); // 实例化User对象
    	$id = $_POST["uID"];
    	$content = $_POST["uContent"];
    	//echo $id;
		// 要修改的数据对象属性赋值
		$data['ID'] = $id;
		$data['Content'] = $content;
		$Note->save($data); // 根据条件保存修改的数据

		$this->redirect('Index/index');

    }
}