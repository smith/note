<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index() {
    	$List = M('list');
    	$notes = $List->select();

    	$this->assign('notes', $notes);
    	$this->display('index');
    }

    public function addNote() {
    	$List = D('list');
    	$List->create();
    	$List->add();

    	$this->redirect('index');
    }

    public function deleteNote() {
        $List = M('list');
        $List->delete($_POST['id']);

        self::_returnJsonp();
    }

    public function updateNote() {
        $List            = M('list');
        $data['id']      = $_POST['id'];
        $data['content'] = $_POST['content'];
        $List->save($data);

        self::_returnJsonp();
    }

    public function _returnJsonp($params=array()) {
        echo $_SERVER['callback'].'('.json_encode($params).')';
    }
}