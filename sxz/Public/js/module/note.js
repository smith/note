
var note = {};

note.init = function() {
	$('.content-zoon').on('click', '.btn', function(e) {
		$('.content-zoon .item').removeClass('editing');

		var $dom = $(e.currentTarget);
		if($dom.hasClass('delete')) {
			var $item = $dom.parents('.item');
			var id    = $item.attr('data-id');
			note.post('deleteNote', {id: id}, function() {
				$item.remove();
			});
			e.stopPropagation();
		}else if($dom.hasClass('item')) {
			var $item = $dom;
			var id    = $item.attr('data-id');
			$item.addClass('editing').find('.item-input').focus();
		}else if($dom.hasClass('ok')) {
			var $item   = $dom.parents('.item');
			var id      = $item.attr('data-id');
			var content = $item.find('.item-input').val();
			note.post('updateNote', {id:id, content:content}, function() {
				$item.find('.item-content').text(content);
			});
			e.stopPropagation();
		}
	});
};

note.post = function(actionName, params, cb) {
	$.post(URL+'/'+actionName, params||{}, cb||function(){});
};