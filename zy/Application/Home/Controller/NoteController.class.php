<?php
namespace Home\Controller;
use Think\Controller;
class NoteController extends Controller {
  public function index() {
  	// echo "To Do List";
  	$Note = D('Note');
  	// var_dump($note);
  	$notes = $Note->select();
  	// $this->ajaxReturn($lists);
  	$this->assign("notes", $notes);
  	$this->display();
  }

  public function add() {
  	$Note = D('Note');
    $Note->create();
    $result = $Note->add();
    // dump($result);
    if ($result) {
      // $this->success('成功', '/Home/Note');
      $this->redirect('/Home/Note');
    } else {
      $this->error('失败');
    }
  }

  public function update() {
    $Note = D('Note');
    $data['id'] = I('psot.id');
    $data['content'] = I('post.content');
    // dump($data);
    $Note->create();
    $Note->save();
    // $this->redirect('/Home/Note');
    $this->ajaxReturn($data);
  }

  public function delete($id) {
  	$Note = D('Note');
    $result = $Note->delete($id);
    if($result) {
      // $this->success('删除Note成功', '/Home/Note', 3);
      $this->redirect('/Home/Note');
    } else {
      // $this->error('删除失败', '/Home/Note', 3);
    }
  }

  public function get($id) {
    $Note = D('Note');
    $r = $Note->find($id);
    $this->ajaxReturn($r);
  }

  public function ajax() {
    $Note = D('Note');
  	$this->ajaxReturn($Note->select());
  }
  public function r() {
    // $this->ajaxReturn('ddddd');
    // $this->success('成功。。。。', U('Home/Note/ajax'));
    // $this->success('登陆成功');
    $this->redirect('/Home/Note');
  }
}
?>